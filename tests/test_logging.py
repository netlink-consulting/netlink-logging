import __main__
__main__.__file__ = __file__

import pytest
from netlink.logging import logger
# noinspection PyProtectedMember
import netlink.logging._logging as src


class TestLogging:
    def test_default_level(self):
        assert logger.level == src.DEFAULT_LEVEL

    def test_set_level(self):
        logger.set_level(5)
        assert logger.level == src.TRACE

    def test_log_critical(self, tmpdir):
        logger.set_file(f'{tmpdir}.critical.log')
        logger.critical('Critical Message')
        logger.set_file()
        with open(f'{tmpdir}.critical.log') as f:
            lines = f.readlines()
            assert len(lines) == 1
            assert lines[0].startswith('[C ')
            assert lines[0].strip().endswith('] Critical Message')

    def test_log_error(self, tmpdir):
        logger.set_file(f'{tmpdir}.error.log')
        logger.error('Error Message')
        logger.set_file()
        with open(f'{tmpdir}.error.log') as f:
            lines = f.readlines()
            assert len(lines) == 1
            assert lines[0].startswith('[E ')
            assert lines[0].strip().endswith('] Error Message')

    def test_log_warning(self, tmpdir):
        logger.set_file(f'{tmpdir}.warning.log')
        logger.warning('Warning Message')
        logger.set_file()
        with open(f'{tmpdir}.warning.log') as f:
            lines = f.readlines()
            assert len(lines) == 1
            assert lines[0].startswith('[W ')
            assert lines[0].strip().endswith('] Warning Message')

    def test_log_success(self, tmpdir):
        logger.set_file(f'{tmpdir}.success.log')
        logger.success('Success Message')
        logger.set_file()
        with open(f'{tmpdir}.success.log') as f:
            lines = f.readlines()
            assert len(lines) == 1
            assert lines[0].startswith('[S ')
            assert lines[0].strip().endswith('] Success Message')

    def test_log_info(self, tmpdir):
        logger.set_file(f'{tmpdir}.info.log')
        logger.info('Info Message')
        logger.set_file()
        with open(f'{tmpdir}.info.log') as f:
            lines = f.readlines()
            assert len(lines) == 1
            assert lines[0].startswith('[I ')
            assert lines[0].strip().endswith('] Info Message')

    def test_log_verbose(self, tmpdir):
        logger.set_file(f'{tmpdir}.verbose.log')
        logger.verbose('Verbose Message')
        logger.set_file()
        with open(f'{tmpdir}.verbose.log') as f:
            lines = f.readlines()
            assert len(lines) == 1
            assert lines[0].startswith('[V ')
            assert lines[0].strip().endswith('] Verbose Message')

    def test_log_debug(self, tmpdir):
        logger.set_file(f'{tmpdir}.debug.log')
        logger.debug('Debug Message')
        logger.set_file()
        with open(f'{tmpdir}.debug.log') as f:
            lines = f.readlines()
            assert len(lines) == 1
            assert lines[0].startswith('[D ')
            assert lines[0].strip().endswith('] Debug Message')

    def test_log_trace(self, tmpdir):
        logger.set_file(f'{tmpdir}.trace.log')
        logger.trace('Trace Message')
        logger.set_file()
        with open(f'{tmpdir}.trace.log') as f:
            lines = f.readlines()
            assert len(lines) == 1
            assert lines[0].startswith('[T ')
            assert lines[0].strip().endswith('] Trace Message')
